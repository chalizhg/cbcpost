Features
===============

The main features of cbcpost are

- Saving in 7 different save formats (xdmf, hdf5, xml, xml.gz, pvd, shelve, txt)
- Plotting using dolfin.plot or pyplot
- Automatic planning of computations, saving and plotting
- Automatic dependency handling
- Many different fields provided, ranging from time integrals to point evaluations and norms.
- Easily expandable with custom :class:`.Field`-subclasses
- Flexible parameter system
- Small footprint on solver code
- Replay functionality
- Restart support
