Field bases
-------------------------------
.. automodule:: cbcpost.fieldbases
    :members:
    :special-members:
    :undoc-members:
    :show-inheritance:
